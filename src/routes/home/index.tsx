import { FunctionalComponent, h } from "preact";
import { useCallback, useRef, useState } from "preact/hooks";
import { DSVRowString } from "d3-dsv";
import { csvParse } from "d3";
import { css } from "@emotion/css";
import Loading from "../../components/Loading";
import BarGraph from "../..//components/BarGraph";

const homeCSS = css`
  padding: 56px 20px;
  min-height: 100%;
  width: 100%;
`;

const groupAndFilterDataByMonth = (
  data: any[],
  kindFilter: string[]
): { keys: string[]; groupedByMonth: { [key: string]: any[] } } => {
  const keys: string[] = [];
  const contributions = data.filter((f) => kindFilter.includes(f.kind));

  const groupedByMonth = contributions.reduce((aggr, f) => {
    const splitDate = f.datetime?.split("-");
    const yearMonth = `${splitDate?.[0] ?? ""}-${splitDate?.[1] ?? ""}`;
    if (isNaN(+(splitDate?.[0] ?? "z"))) {
      return aggr;
    }
    if (yearMonth && aggr[yearMonth]) {
      aggr[yearMonth].push(f);
    } else {
      keys.push(yearMonth);
      aggr[yearMonth] = [f];
    }
    return aggr;
  }, {} as { [key: string]: DSVRowString<string>[] });
  const sortedKeys = keys.sort((a, b) => (a > b ? 1 : -1));

  return { keys: sortedKeys, groupedByMonth };
};

export type DateData = {
  date: string;
  amount: number;
};

const Home: FunctionalComponent = () => {
  const reader = useRef(new FileReader());
  const [loading, setLoading] = useState(false);
  const [monthlyContributionsVolume, setMonthlyContributionsVolume] =
    useState<DateData[]>();
  const [recurringContributionsVolume, setRecurringContributionsVolume] =
    useState<DateData[]>();
  const [monthlyContributionsTotal, setMonthlyContributionsTotal] =
    useState<DateData[]>();
  const [recurringContributionsTotal, setRecurringContributionsTotal] =
    useState<DateData[]>();
  const [monthlyExpensesTotal, setMonthlyExpensesTotal] =
    useState<DateData[]>();
  const [monthlyAverageContributions, setMonthlyAverageContributions] =
    useState<DateData[]>();
  const [monthlyMedianContributions, setMonthlyMedianContributions] =
    useState<DateData[]>();
  const [collectiveURL, setCollectiveURL] = useState(
    new URLSearchParams(window.location.search).get("url") ?? ""
  );

  const dataProcess = useCallback(async (e: any) => {
    const data = csvParse(e.target.result);
    const { groupedByMonth: contributionsByMonth, keys } =
      groupAndFilterDataByMonth(data, ["CONTRIBUTION", "ADDED_FUNDS"]);
    console.log("data", data);
    const { groupedByMonth: expensesByMonth } = groupAndFilterDataByMonth(
      data,
      ["EXPENSE"]
    );

    setMonthlyContributionsVolume(
      keys.map((k) => {
        return { date: k, amount: contributionsByMonth[k].length };
      })
    );
    const totalContributions = keys.map((k) => {
      return {
        date: k,
        amount: contributionsByMonth[k].reduce(
          (aggr, datum) =>
            datum.netAmount && !isNaN(+datum.netAmount ?? "0")
              ? aggr + +datum.netAmount
              : aggr,
          0
        ),
      };
    });
    setMonthlyContributionsTotal(totalContributions);
    setRecurringContributionsVolume(
      keys.map((k) => {
        return {
          date: k,
          amount: contributionsByMonth[k].filter((c) =>
            c.description.startsWith("Monthly")
          ).length,
        };
      })
    );
    const totalRecurringContributions = keys.map((k) => {
      return {
        date: k,
        amount: contributionsByMonth[k]
          .filter((c) => c.description.startsWith("Monthly"))
          .reduce(
            (aggr, datum) =>
              datum.netAmount && !isNaN(+datum.netAmount ?? "0")
                ? aggr + +datum.netAmount
                : aggr,
            0
          ),
      };
    });
    setRecurringContributionsTotal(totalRecurringContributions);
    setMonthlyAverageContributions(
      keys.map((k, i) => {
        return {
          date: k,
          amount: totalContributions[i].amount / contributionsByMonth[k].length,
        };
      })
    );
    setMonthlyMedianContributions(
      keys.map((k) => {
        return {
          date: k,
          amount: +contributionsByMonth[k].sort((a, b) => {
            return a.netAmount > b.netAmount ? -1 : 1;
          })[Math.floor(contributionsByMonth[k].length / 2)].netAmount,
        };
      })
    );
    setMonthlyExpensesTotal(
      keys.map((k) => {
        return {
          date: k,
          amount:
            expensesByMonth[k]?.reduce(
              (aggr, datum) =>
                datum.netAmount && !isNaN(+datum.netAmount ?? "0")
                  ? aggr + +datum.netAmount
                  : aggr,
              0
            ) ?? 0,
        };
      })
    );
  }, []);

  const changeHandler = useCallback(
    (event: any) => {
      const file = event.target.files[0];

      reader.current.addEventListener("load", dataProcess, false);
      if (file) {
        reader.current.readAsText(file);
      }
    },
    [dataProcess]
  );

  const onClickFetch = useCallback(async () => {
    setLoading(true);
    if (collectiveURL && collectiveURL !== "") {
      const removeHash = collectiveURL.split("#");
      const split = removeHash[0].split("/");
      const collectiveSlug = split[3];
      const url = `https://rest.opencollective.com/v2/${collectiveSlug}/transactions.txt?kind=ADDED_FUNDS,BALANCE_TRANSFER,CONTRIBUTION,EXPENSE,PLATFORM_TIP&includeGiftCardTransactions=1&includeIncognitoTransactions=1&includeChildrenTransactions=1&fetchAll=1`;
      const response = await fetch(url);
      const result = await response.text();
      await dataProcess({ target: { result } });
    }
    setLoading(false);
  }, [collectiveURL, dataProcess]);

  console.log("monthlyMedian", monthlyMedianContributions);

  return (
    <div className={homeCSS}>
      <div
        className={css`
          margin: 0 auto;
          display: flex;
          align-items: center;
          flex-direction: column;
          padding: 2rem 0.5rem;
        `}
      >
        <div
          className={css`
            margin-bottom: 2rem;
            max-width: 400px;
            text-align: center;
            display: flex;
            align-items: center;
            width: 100%;
            flex-direction: column;

            & input {
              font-size: 1.3rem;
              width: 100%;
            }
          `}
        >
          <label>
            This super simple website turns an Open Collective transactions file
            into a couple of graphs, hopefully so that you can better understand
            some patterns in your donations. You can download your transactions
            from your budget section. Click on "All transactions", and then
            "Download CSV" on the right hand side. Then click on the v2 CSV and
            import it.
          </label>
          <input type="file" name="file" onChange={changeHandler} />
        </div>
        <div
          className={css`
            display: flex;
            justify-content: center;
            width: 100%;
            @media screen and (max-width: 500px) {
              flex-direction: column;
            }
            input,
            button {
              font-size: 1.3rem;
            }

            button {
              cursor: pointer;
            }
          `}
        >
          <input
            type="text"
            placeholder="Open Collective URL"
            onChange={(e: any) => setCollectiveURL(e.target.value)}
            value={collectiveURL}
          />
          <button type="button" onClick={onClickFetch} disabled={loading}>
            fetch
          </button>
        </div>
      </div>
      <div
        className={css`
          display: flex;
          margin-top: 4rem;
          flex-direction: column;
          align-items: center;
        `}
      >
        {loading && <Loading />}
        {!loading && monthlyContributionsVolume && (
          <BarGraph
            label="Total number of contributions each month"
            data={monthlyContributionsVolume}
          />
        )}
        {!loading && monthlyContributionsTotal && (
          <BarGraph
            label="Total dollar amount received each month from contributions"
            data={monthlyContributionsTotal}
            amountTransform={(d) =>
              d.amount > 1000
                ? `$${(d.amount / 1000).toFixed(1)}k`
                : `$${d.amount.toFixed(0)}`
            }
          />
        )}
        {!loading && recurringContributionsVolume && (
          <BarGraph
            label="Total number of contributions each month from recurring contributions"
            data={recurringContributionsVolume}
          />
        )}
        {!loading && recurringContributionsTotal && (
          <BarGraph
            label="Total dollar amount received each month from recurring contributions"
            data={recurringContributionsTotal}
            amountTransform={(d) =>
              d.amount > 1000
                ? `$${(d.amount / 1000).toFixed(1)}k`
                : `$${d.amount.toFixed(0)}`
            }
          />
        )}
        {!loading && monthlyAverageContributions && (
          <BarGraph
            label="Average dollar amount of a contribution each month"
            data={monthlyAverageContributions}
            amountTransform={(d) => `$${d.amount.toFixed(1)}`}
          />
        )}
        {!loading && monthlyMedianContributions && (
          <BarGraph
            label="Median dollar amount of a contribution each month"
            data={monthlyMedianContributions}
            amountTransform={(d) => `$${d.amount.toFixed(1)}`}
          />
        )}
        {!loading && monthlyExpensesTotal && (
          <BarGraph
            label="Total dollar amount expended each month from contributions"
            data={monthlyExpensesTotal}
            amountTransform={(d) =>
              d.amount > 1000
                ? `$${(d.amount / 1000).toFixed(1)}k`
                : `$${d.amount.toFixed(0)}`
            }
            color="red"
          />
        )}
      </div>
    </div>
  );
};

export default Home;
