import { FunctionalComponent, h } from "preact";
import { Route, Router } from "preact-router";
import { createHashHistory } from "history";

import Home from "../routes/home";
import NotFoundPage from "../routes/notfound";
import Header from "./header";

const App: FunctionalComponent = () => {
  return (
    <div id="preact_root">
      <Header />
      <Home />
      {/* <Router history={createHashHistory()}> */}
      {/* <Route path="/" component={Home} /> */}
      {/* <NotFoundPage default /> */}
      {/* </Router> */}
    </div>
  );
};

export default App;
