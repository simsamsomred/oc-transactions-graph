import { css } from "@emotion/css";
import { max, scaleLinear } from "d3";
import { h, FunctionalComponent } from "preact";
import { DateData } from "src/routes/home";

const graphCSS = css`
  display: flex;
  flex-direction: column;
  margin-bottom: 2rem;
  max-width: 100%;
  overflow-y: scroll;
`;

const rectCSS = css`
  transition: 0.5s fill;
  &:hover {
    fill: darkslateblue;
  }
`;

const labelCSS = css`
  font-weight: bold;
`;

export const BarGraph: FunctionalComponent<{
  label: string;
  data: DateData[];
  amountTransform?: (d: DateData) => string;
  color?: string;
}> = ({ label, data, amountTransform, color = "lightblue" }) => {
  const keyHeight = 75;
  const barWidth = 25;

  const monthlyVolumeMax = max(data ?? [], (d) => Math.abs(d.amount));

  const scaleVolume = scaleLinear()
    .domain([0, monthlyVolumeMax ?? 0])
    .range([0, 300]);

  const spacer = 10;

  return (
    <div className={graphCSS}>
      <label className={labelCSS}>{label}</label>
      <svg width={(barWidth + spacer) * (data.length + 1)} height="400px">
        {data?.map((v, i) => (
          <g key={v.date}>
            <rect
              width={barWidth}
              x={i * (barWidth + spacer)}
              y={400 - scaleVolume(Math.abs(v.amount)) - keyHeight}
              height={scaleVolume(Math.abs(v.amount))}
              className={rectCSS}
              fill={color}
            />
            <text
              x={i * (barWidth + spacer)}
              y={400}
              transform={`rotate(45, ${i * (barWidth + spacer) + keyHeight}, ${
                400 - 25
              })`}
            >
              {v.date}
            </text>
            <text
              x={i * (barWidth + spacer)}
              style={{ fontSize: "12px" }}
              y={400 - scaleVolume(Math.abs(v.amount)) - keyHeight - spacer}
            >
              {amountTransform ? amountTransform(v) : v.amount}
            </text>
          </g>
        ))}
      </svg>
    </div>
  );
};

export default BarGraph;
