import { FunctionalComponent, h } from "preact";
import { css } from "@emotion/css";

const headerCSS = css`
  position: fixed;
  left: 0;
  top: 0;
  width: 100%;
  height: 56px;
  padding: 0;
  background: lightblue;
  box-shadow: 0 0 5px rgba(0, 0, 0, 0.5);
  z-index: 50;

  h1 {
    float: left;
    margin: 0;
    padding: 0 15px;
    font-size: 24px;
    line-height: 56px;
    font-weight: 400;
    color: darkslateblue;
  }
`;

const Header: FunctionalComponent = () => {
  return (
    <header class={headerCSS}>
      <h1>OC Visualized</h1>
    </header>
  );
};

export default Header;
